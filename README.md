# Poller with FastAPI

This project is an example of how to use a background **poller** process to keep
value objects in one service synced up with their source entities in another
service.

## Dev setup

```bash
docker volume create poller-data
docker-compose build
docker-compose up
```

## Deployment todos:

Just 5 **short** steps 😸

* create 3 apps on Heroku to use (inventory, sales, sales-poller)
* add a Postgres database onto the sales service
* create Heroku "Config Vars" in the **sales-poller** app:
  * DATABASE_URL: copy the value from the sales Service
  * INVENTORY_API_HOST: the Heroku hostname of the inventory service (ex:
  https://inventory-app.herokuapp.com) (NO SLASH at the end❗❗❗)
* create GitLab CI/CD variables:
  * HEROKU_API_KEY: Your API Key from Heroku account settings
  * HEROKU_INVENTORY_APP: The name of the inventory app in Heroku
  * HEROKU_SALES_APP: The name of the sales app in Heroku
  * HEROKU_SALES_POLLER_APP: The name of the sales-poller app in Heroku
* set the sales-poller app to be a "worker" app in Heroku:
  * Go to: sales-poller app -> Resources tab
  * Turn off the "web" option
  * Turn on the "worker" option

## Project overview

The application is composed of 3 services plus a database.

The **Inventory** service is the owning service of the Automobile entity and it
has an API endpoint for getting all of the autos in the inventory.

The **Sales** service needs to reference the Automobiles from the **Inventory**
and it uses AutoVO value objects to refer to them.

The **Sales Poller** service exists solely to keep the AutoVOs in the **Sales**
service in sync with the Automobiles in the **Inventory** service.

## File organization

All of the poller specific files except for its Dockerfiles are in the
`sales/poller` directory. The poller usually has one or more dependencies on
it's "parent" service's code, the **Sales** service in this case, so the
poller's Dockerfiles: `Dockerfile-poller` and `Dockerfile-poller.dev` are in the
`sales` directory so that Docker has access to all of the code that it needs to
build the image for the poller.

## The poller Dockerfiles

### `Dockerfile-poller.dev`

Line 2, below, ensures that the output from the poller is forwarded to the
log-file as it is created. If this isn't here you might not be able to see
the output from poller when you are debugging it ;-)

```dockerfile
ENV PYTHONUNBUFFERED 1
```

On line 10, below, the `PYTHONPATH` environment variable is set to "/app".

```dockerfile
ENV PYTHONPATH /app
```

This is so that when `db.py` or other files from **Sales** are needed in `poller.py`
they can easily be imported like:

```python
from db import AutoVOQueries
```

### `Dockerfile-poller`

This file has the same line #2 as above.

After `pip` is upgraded, all of the files needed by the poller are `COPY`ed to the
`/app` directory.

After that the Python dependencies are installed and the image is setup to run
`python poller.py` when it is started up.
