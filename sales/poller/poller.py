import os
from time import sleep

import requests
from db import AutoVOQueries

inventory_url = os.environ["INVENTORY_API_HOST"] + "/api/inventory"
POLL_INTERVAL_SECS = int(os.environ.get("POLL_INTERVAL_SECS", 60))

def poll():
    queries = AutoVOQueries()

    while True:
        print("the poller is running")
        try:
            response = requests.get(inventory_url)
            if response.status_code == 200:
                print("data from inventory:", response)
                autos = response.json()
                for auto in autos:
                    queries.create_autovo(auto)

        except Exception as ex:
            print("Exception caught in poller", ex)

        sleep(POLL_INTERVAL_SECS)

if __name__ == "__main__":
    poll()